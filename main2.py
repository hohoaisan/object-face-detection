
# from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
# import pickle
import cv2
import dlib

modelFile = "model/opencv_face_detector_uint8.pb"
configFile = "model/opencv_face_detector.pbtxt"
net =    cv2.dnn.readNetFromTensorflow(modelFile, configFile)


cap = cv2.VideoCapture('rtsp://admin:admin@192.168.1.3:8554/live')
# cap = imutils.video.VideoStream('rtsp://admin:admin@192.168.1.3:8554/live').start()
fps = FPS().start()

while True:
  flags, frame = cap.read()
  # frame = cap.read()
  frame = imutils.resize(frame, width=600)
  # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
  # 
  (h, w) = frame.shape[:2]
  imageBlob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0, (300, 300),
  (104.0, 177.0, 123.0), swapRB=False, crop=False)
  net.setInput(imageBlob)
  detections = net.forward()

  cv2.imshow('img', frame)
  key = cv2.waitKey(1) & 0xFF
  if key == ord("q"):
    break   
cap.release()
cv2.destroyAllWindows() 