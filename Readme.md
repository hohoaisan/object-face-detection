For implementing-:
> Open the command prompt
> navigate to folder (inside the folder)
> enter-:
Installing environment, can be skipped if it was installed-:
```
conda create -n face_detection python=3.6
```
Activate the environment on Windows-:
```
conda activate face_detection
```
Installing the required dependencies-:
```
pip install opencv-python
conda install -c menpo dlib
pip install imutils
```
Detecting you face in relative through video feed from you webcam -:
Remember to change video stream source before running.

```
main.py
```


Detecting face in an image ( already an image in present in the images folder)
```
detection_tensorflow_image.py
```
if you want your face detected for a your image-:
a) place your image , in the images folder with names as test
b) or replace the word test with you image file name placed in the images folder , in the 9th line of code of face_detect_image.py file
